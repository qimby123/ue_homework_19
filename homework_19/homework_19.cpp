﻿// homework_19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <time.h> 


enum ANIMAL { DOG, CAT, COW };

class Animal {
public:
    Animal() {}
    virtual void Voice() = 0;
    static Animal** createRandAnimals(const int count_animals);
};

class Dog : public Animal {
public:
    void Voice() {
        std::cout << "Wouf!\n";
    }
};

class Cat : public Animal {
public:

    void Voice() {
        std::cout << "Meow!\n";
    }
};

class Cow : public Animal {
public:
    void Voice() {
        std::cout << "Moo!\n";
    }
};

Animal** Animal::createRandAnimals(const int count_animals) {
    Animal** animals = new Animal*[count_animals];

    for (int i = 0; i < count_animals; i++) {
        int type_animal = rand() % 3;

        switch (type_animal)
        {
        case DOG:
            animals[i] = new Dog();
            break;
        case CAT:
            animals[i] = new Cat();
            break;
        case COW:
            animals[i] = new Cow();
            break;
        default:
            break;
        }
    }

    return animals;
}

int main()
{
    std::srand( time(NULL) );

    const int count_animals = 20;

    Animal** rand_animals = Animal::createRandAnimals(count_animals);

    for(auto a = rand_animals; a < rand_animals + count_animals; a++)
        (*a)->Voice();
}
